# postfix-test

This is a TLS enabled dummy postfix servers.  It discards any mail sent to it.

If you want to check the received message you can send to `save@[hostname]` and it will save messages to `/var/log/mail.output`

If you want the message to bounce send it to `bounce@[hostname]` and it will 550 bounce.