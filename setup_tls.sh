#!/usr/bin/env bash
MY_HOSTNAME=$(hostname);
cd /etc/ssl/private;
# genrsa -des3 -out localhost.key 2048;
# openssl req -new -key localhost.key -out localhost.csr;
# openssl x509 -req -days 365 -in localhost.csr -signkey localhost.key -out localhost.crt;

echo "setting up ssl with hostname: ${MY_HOSTNAME}";
openssl req \
    -new \
    -newkey rsa:4096 \
    -days 365 \
    -nodes \
    -x509 \
    -subj "/C=US/ST=NC/L=Asheville/O=massmail/CN=${MY_HOSTNAME}" \
    -keyout localhost.key \
    -out localhost.cert;

cat localhost.key localhost.cert > localhost.pem;

echo ${MY_HOSTNAME} > /etc/mailname;
echo "set postfix mailname to: $(cat /etc/mailname)";