#!/usr/bin/env bash
/root/setup_tls.sh;
/usr/sbin/rsyslogd;
/usr/sbin/postfix start 2>&1;

#we go too fast and mail.log doesn't exist yet
sleep 1;
tail -F /var/log/maillog;